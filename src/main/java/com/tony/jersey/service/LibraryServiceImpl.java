/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author tony
 *
 */
@Service
@Path("library")
public class LibraryServiceImpl implements LibraryService {

	
	@GET
	@Path("/books")
	//@Consumes("text/*")
	@Override
	public String getBooks() {
		return "books";
	}

	@Override
	@GET
	@Path("/book/{isbn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@PathParam("isbn") ISBN id) {//ISBN需要带一个String参数的构造方法
		Book book = new Book();
		book.setId(id);
		book.setName("java book");
		return book;
	}
	

	@Override
	@POST
	@Path("/book")
	public void addBook(Book book) {//实体参数
		System.out.println("name:" + book.getName());
		System.out.println("id:" + book.getId());
	}

	@DELETE
	@Path("/book")
	public void removeBook(@QueryParam("id") String id) {
		System.out.println("remove id:" + id);
	}

}
