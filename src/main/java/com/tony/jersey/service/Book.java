/**
 * 
 */
package com.tony.jersey.service;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author tony
 *
 */
//@XmlRootElement此注解不能少
@XmlRootElement
public class Book {
	ISBN id;
	String name;
	
	/*public Book(ISBN id, String name){
		this.id = id;
		this.name = name;
	}*/
	
	//@XmlElement(name="ids")
	public ISBN getId() {
		return id;
	}
	public void setId(ISBN id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
