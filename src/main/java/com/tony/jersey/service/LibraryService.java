/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * @author tony
 *
 */

public interface LibraryService {
	public String getBooks();
	public Book getBook(ISBN id);
	public void addBook(Book book);
	public void removeBook(@QueryParam("id") String id);
}
