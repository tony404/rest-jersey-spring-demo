/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author tony
 *
 */
@Path("school")
public class SchoolService {
	@GET
	@Path("/school")
	public String getBooks() {
		return "school";
	}
}
